function fn() {
  var env = karate.env;
  if (!env) {
    env = 'dev';
  }
  karate.log('karate.env system property was:', env);
  var config = {
    appId: 'my.app.id',
    appSecret: 'my.secret',
    goRestUrlBase: 'https://gorest.co.in/public/v2/',
    tokenGoRest: 'Bearer 48e2d2aef17529ed4620a87beca3e2556a380263f70a5487ab0da17357c77d97'
  };
  if (env == 'stage') {
    config.someUrlBase = 'https://stage-host/v1/auth';
  } else if (env == 'e2e') {
    config.someUrlBase = 'https://e2e-host/v1/auth';
  }
  //karate.configure('connectTimeout', 5000);
  //karate.configure('readTimeout', 5000);
  return config
}