Feature: get registered users on the goRest platform

  Background:
    * url goRestUrlBase
    * header Authorization = tokenGoRest
    * def jsonFile = read('../../users/modelUserKarate.json')

  @smokeTest
  Scenario: Get all user and validate their field
    Given path '/users'
    When method get
    Then status 200
    * print response
    * match each response == jsonFile
    #Alternative
#    * set modelUser
#      | path          | value                                |
#      | id            | '#number'                            |
#      | id            | '#notnull'                           |
#      | email         | '#string'                            |
#      | email         | '#regex ^[^@]+@[^@]+\.[a-zA-Z]{2,}$' |
#      | gender        | '#regex ^(male\|female)$'            |
#      | status        | '#regex ^(active\|inactive)$'        |
#      | name          | '#notnull'                           |
#      | name          | '#string'                            |
#      | optionalPhone | '##number'                           |
#    * match each response == modelUser