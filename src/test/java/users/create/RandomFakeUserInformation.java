package users.create;

import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class RandomFakeUserInformation {
    Faker faker = Faker.instance(new Locale("en", "US"), new Random());
    List<String> gender = new ArrayList<>();
    List<String> status = new ArrayList<>();

    public ModelCreateUser createNewUser() {
        ModelCreateUser modelCreateUser = new ModelCreateUser();
        modelCreateUser.setEmail(faker.internet().emailAddress());
        modelCreateUser.setName(faker.name().firstName());
        gender.add("male");
        gender.add("female");
        modelCreateUser.setGender(gender.get(faker.number().numberBetween(0, 2)));
        status.add("active");
        status.add("inactive");
        modelCreateUser.setStatus(status.get(faker.number().numberBetween(0, 2)));
        return modelCreateUser;
    }
}
