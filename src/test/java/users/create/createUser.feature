Feature: creation of users

  Background:
    * url goRestUrlBase
    * header Authorization = tokenGoRest
    * def dataFake = Java.type('users.create.RandomFakeUserInformation')
    * def df = new dataFake();
    * def req =
                """
                {
                name:#(df.createNewUser().getName()),
                email:#(df.createNewUser().getEmail()),
                gender:#(df.createNewUser().getGender()),
                status:#(df.createNewUser().getStatus())
                }
                """
    * def jsonFile = read('../../users/modelUserKarate.json')

    @smokeTest
  Scenario: create a new user with non-existing mail form 1-with table
    Given path '/users'
    And request req
    When method post
    Then status 201
    * set modelNewUser
      | path   | value                                |
      | id     | '#number'                            |
      | id     | '#notnull'                           |
      | name   | '#notnull'                           |
      | name   | '#string'                            |
      | email  | '#string'                            |
      | email  | '#regex ^[^@]+@[^@]+\.[a-zA-Z]{2,}$' |
      | gender | '#regex ^(male\|female)$'            |
      | status | '#regex ^(active\|inactive)$'        |

    And match response == modelNewUser

  Scenario: create a new user with non-existing mail form 2-with json file
    Given path '/users'
    And request req
    When method post
    Then status 201
    And match response == jsonFile

  @smokeTest
  Scenario: create a new user with existing email
    Given path '/users'
    And request req
    And method post
    #email already exists
    * header Authorization = tokenGoRest
    Given path '/users'
    When request req
    And method post
    Then status 422
    * set modelResponse
      | path    | value                    |
      | field   | 'email'                  |
      | message | 'has already been taken' |
    And match each response == modelResponse

  Scenario Outline: create users without including any field in the body
    Given path '/users'
    * def body =
                """
                {
                name:<name>,
                email:<email>,
                gender:<gender>,
                status:<statusBody>
                }
                """

    When request body
    And method post
    Then status 422
    And match each response == <responseCase>

    Examples:
      | email                              | gender                              | statusBody                          | name                              | responseCase                                  |
      | "#(df.createNewUser().getEmail())" | "#(df.createNewUser().getGender())" | "#(df.createNewUser().getStatus())" | null                              | {"field":"name","message":"can't be blank"}   |
      | "#(df.createNewUser().getEmail())" | "#(df.createNewUser().getGender())" | null                                | "#(df.createNewUser().getName())" | {"field":"status","message":"can't be blank"} |
      | "#(df.createNewUser().getEmail())" | null                                | "#(df.createNewUser().getStatus())" | "#(df.createNewUser().getName())" | {"field":"gender","message":"can't be blank"} |
      | null                               | "#(df.createNewUser().getGender())" | "#(df.createNewUser().getStatus())" | "#(df.createNewUser().getName())" | {"field":"email","message":"can't be blank"}  |

