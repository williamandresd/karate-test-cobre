package users;

import com.intuit.karate.junit5.Karate;

public class UserTest {
    @Karate.Test
    Karate testCreateUser() {
        return Karate.run("create/createUser").relativeTo(getClass());
    }

    @Karate.Test
    Karate testSmokeTest() {
        return Karate.run().tags("@smokeTest").relativeTo(getClass());
    }

    @Karate.Test
    Karate testAll() {
        return Karate.run().relativeTo(getClass());
    }
}
